<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.hspconsortium.reference</groupId>
        <artifactId>hspc-reference-auth-parent</artifactId>
        <version>1.17.22-SNAPSHOT</version>
        <relativePath>../pom.xml</relativePath>
    </parent>

    <artifactId>hspc-reference-auth-server-webapp</artifactId>
    <packaging>war</packaging>

    <name>Reference Auth - Web App</name>
    <description>Artifacts for creating an HSPC compliant authorization server web app</description>
    <url>https://healthservices.atlassian.net/wiki/display/HSPC/Healthcare+Services+Platform+Consortium</url>

    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/hspconsortium/reference-auth-server-webapp.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/hspconsortium/reference-auth-server-webapp.git
        </developerConnection>
        <url>https://bitbucket.org/hspconsortium/reference-auth-server-webapp.git</url>
        <tag>HEAD</tag>
    </scm>

    <dependencies>

        <!-- mitreid dependencies -->
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>2.3.0</version>
        </dependency>
        <dependency>
            <groupId>com.sun.xml.bind</groupId>
            <artifactId>jaxb-core</artifactId>
            <version>2.3.0</version>
        </dependency>
        <dependency>
            <groupId>com.sun.xml.bind</groupId>
            <artifactId>jaxb-impl</artifactId>
            <version>2.3.0</version>
        </dependency>
        <dependency>
            <groupId>javax.activation</groupId>
            <artifactId>activation</artifactId>
            <version>1.1</version>
        </dependency>

        <dependency>
            <groupId>org.mitre</groupId>
            <artifactId>openid-connect-parent</artifactId>
            <type>pom</type>
            <version>${mitreid.version}</version>
        </dependency>
        <dependency>
            <groupId>org.mitre</groupId>
            <artifactId>openid-connect-server-webapp</artifactId>
            <type>war</type>
            <version>${mitreid.version}</version>
        </dependency>
        <dependency>
            <groupId>org.mitre</groupId>
            <artifactId>openid-connect-common</artifactId>
            <version>${mitreid.version}</version>
        </dependency>
        <dependency>
            <groupId>org.mitre</groupId>
            <artifactId>openid-connect-server</artifactId>
            <version>${mitreid.version}</version>
        </dependency>

        <dependency>
            <groupId>org.hspconsortium.reference</groupId>
            <artifactId>hspc-reference-auth-security</artifactId>
            <version>${project.version}</version>
        </dependency>

        <!-- some dependencies provided by the MITREid overlay, but declared here for the IDE -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>2.5</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.zaxxer</groupId>
            <artifactId>HikariCP</artifactId>
            <version>2.6.1</version>
            <scope>provided</scope>
        </dependency>

        <!-- pegging spring versions for certain artifacts to avoid duplicate imports -->
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-config</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-web</artifactId>
        </dependency>
        <dependency>
            <artifactId>spring-jdbc</artifactId>
            <groupId>org.springframework</groupId>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-ldap</artifactId>
        </dependency>

        <!-- property encryption -->
        <dependency>
            <groupId>org.jasypt</groupId>
            <artifactId>jasypt-spring31</artifactId>
            <version>1.9.2</version>
        </dependency>

        <!-- enable mysql as our relational datastore -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.18</version>
        </dependency>

        <dependency>
            <groupId>io.jsonwebtoken</groupId>
            <artifactId>jjwt</artifactId>
            <version>0.7.0</version>
        </dependency>

        <dependency>
            <groupId>org.pojomatic</groupId>
            <artifactId>pojomatic</artifactId>
            <version>2.2.1</version>
        </dependency>

        <dependency>
            <groupId>org.jacoco</groupId>
            <artifactId>jacoco-maven-plugin</artifactId>
            <version>0.8.1</version>
        </dependency>

        <!-- test -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>1.10.19</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.jayway.jsonpath</groupId>
            <artifactId>json-path-assert</artifactId>
            <version>0.8.1</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>3.2.14.RELEASE</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>javax.inject</groupId>
            <artifactId>javax.inject</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.annotation</groupId>
            <artifactId>javax.annotation-api</artifactId>
            <version>1.3.2</version>
        </dependency>

        <!-- Flyway for database migrations -->
        <dependency>
            <groupId>org.flywaydb</groupId>
            <artifactId>flyway-core</artifactId>
            <version>4.2.0</version>
        </dependency>

        <!-- Firebase -->
        <dependency>
            <groupId>com.google.firebase</groupId>
            <artifactId>firebase-admin</artifactId>
        </dependency>

        <dependency>
            <groupId>org.keycloak</groupId>
            <artifactId>keycloak-spring-security-adapter</artifactId>
        </dependency>

        <dependency>
            <groupId>com.googlecode.json-simple</groupId>
            <artifactId>json-simple</artifactId>
            <version>1.1.1</version>
        </dependency>

        <!--&lt;!&ndash;Solves JpaTransactionManager class not found&ndash;&gt;-->
        <!--<dependency>-->
            <!--<groupId>org.springframework</groupId>-->
            <!--<artifactId>spring-orm</artifactId>-->
            <!--<version>5.2.0.RELEASE</version>-->
        <!--</dependency>-->

        <!--&lt;!&ndash;Solves PersistenceProvider  &ndash;&gt;-->
        <!--<dependency>-->
            <!--<groupId>org.eclipse.persistence</groupId>-->
            <!--<artifactId>org.eclipse.persistence.jpa</artifactId>-->
            <!--<version>2.7.4</version>-->
        <!--</dependency>-->

    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>2.1.1</version>
                <configuration>
                    <overlays>
                        <overlay>
                            <groupId>org.mitre</groupId>
                            <artifactId>openid-connect-server-webapp</artifactId>
                        </overlay>
                    </overlays>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-maven-plugin</artifactId>
                <version>${jetty.version}</version>
                <configuration>
                    <jettyXml>src/main/resources/jetty.xml</jettyXml>
                    <webAppConfig>
                        <allowDuplicateFragmentNames>true</allowDuplicateFragmentNames>
                        <!--<contextPath>/auth</contextPath>-->
                    </webAppConfig>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.4</version>
                <executions>
                    <execution>
                        <id>coverage-initialize</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>coverage-report</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                    <!-- Threshold -->
                    <execution>
                        <id>coverage-check</id>
                        <goals>
                            <goal>check</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <rule>
                                    <element>CLASS</element>
                                    <limits>
                                        <limit>
                                            <counter>LINE</counter>
                                            <value>COVEREDRATIO</value>
                                            <minimum>0.00</minimum>
                                        </limit>
                                    </limits>
                                </rule>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>2.3</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>org.eclipse.jetty</groupId>
                                    <artifactId>jetty-runner</artifactId>
                                    <version>${jetty.version}</version>
                                    <destFileName>jetty-runner.jar</destFileName>
                                </artifactItem>
                            </artifactItems>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

<!--            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>2.22.0</version>
                <executions>
                    <execution>
                        <id>integration-test</id>
                        <goals>
                            <goal>integration-test</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>verify</id>
                        <goals>
                            <goal>verify</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
-->

            <!-- Used for unit tests -->
<!--
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M3</version>
                <configuration>
                    <argLine>${surefireArgLine} -Xmx1024m</argLine>
                    <excludes>
                        <exclude>**/IT*.java</exclude>
                    </excludes>
                </configuration>
            </plugin>
-->
            <plugin>
                <artifactId>maven-jar-plugin</artifactId>
                <configuration>
                    <archive>
                        <manifestFile>WEB-INF/MANIFEST.MF</manifestFile>
                        <manifest>
                            <addClasspath>true</addClasspath>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
